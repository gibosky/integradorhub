package br.com.tolife.integrador_hub.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.tolife.integrador_hub.bean.IntegracaoPaciente;
import br.com.tolife.integrador_hub.bean.LogEventos;
import br.com.tolife.integrador_hub.bean.PacienteHub;
import br.com.tolife.integrador_hub.dao.HubDAO;
import br.com.tolife.integrador_hub.dao.IntegracaoPacienteDAO;

public class FacadeIntegracao {
	
  public FacadeIntegracao() {
		/*if (logEventosDAO == null) {
			logEventosDAO = new LogEventosDAO(LogEventos.class);
		}*/
	}

private HubDAO hubDao = new HubDAO(PacienteHub.class);
  private IntegracaoPacienteDAO integracaoPacienteDAO;
  //private LogEventosDAO logEventosDAO;
  
  private void inicializarDAO(){
	  
	  if(hubDao == null){
		  hubDao = new HubDAO(PacienteHub.class);
	  }
	  
	  if(integracaoPacienteDAO == null){
		  integracaoPacienteDAO = new IntegracaoPacienteDAO(IntegracaoPaciente.class);
	  }
	/*  if (logEventosDAO == null) {
			logEventosDAO = new LogEventosDAO(LogEventos.class);
	  }*/
  }
  
  public void integrar() throws Exception{
	  inicializarDAO();
	  String dataHoraUltimaInternacao = integracaoPacienteDAO.obterUltimaDataHoraInternacao();
	  List<PacienteHub> pacientesHub = hubDao.buscarAtendimento(dataHoraUltimaInternacao);
	  salvarPacienteEmerges(converterIntegracaoPaciente(pacientesHub));
	  Date maxDataHoraUltimaInternacao = integracaoPacienteDAO.obterMaxDataHoraInternacao();
	  if(maxDataHoraUltimaInternacao != null){
		  integracaoPacienteDAO.atualizarUltimaDataHoraParamento(maxDataHoraUltimaInternacao);
	  }
  }
  
  public void salvarLog(LogEventos log){
	//  logEventosDAO.salvar(log);
  }
  
  private void salvarPacienteEmerges(List<IntegracaoPaciente> pacientes){
	  for(IntegracaoPaciente p : pacientes){
		  integracaoPacienteDAO.salvar(p); 
	  }
  }
  
  private List<IntegracaoPaciente> converterIntegracaoPaciente(List<PacienteHub> ps){
	  IntegracaoPaciente p;
	  List<IntegracaoPaciente> pacienteTolife = new ArrayList<IntegracaoPaciente>();
	  //System.out.println("Conversao de paciente HUB para paciente Integracao Paciente");
	  for(PacienteHub item : ps){
		  p = new IntegracaoPaciente();
		  System.out.println("[IntegradorHUB] Codigo paciente: " + String.valueOf(item.getCodigoPac()));
		  p.setIdPaciente(String.valueOf(item.getCodigoPac()));
		  p.setProntuario(item.getProntuario());
		  p.setCartaoSUS(item.getSus());
		  p.setNome(item.getNomePaciente()==null?"M�e "+item.getNomePaciente():item.getNomePaciente());
		  p.setRaca(item.getRaca());
		  p.setDataNascimento(item.getDataNascimento());
		  p.setIdade(item.getIdade());
		  p.setSexo(item.getSexo().toString().substring(0, 1));
		  p.setNomePai(item.getPai());
		  p.setNomeMae(item.getMae());
		  p.setLogradouro(item.getLogradouro());
		  p.setNumero(Integer.getInteger(item.getNumero()));
		  p.setComplemento(item.getComplemento());
		  p.setBairro(item.getBairro());
		  p.setCidade(item.getCidade());
		  p.setEstado(item.getUf());
		  p.setCep(item.getCep());
		  p.setTelefoneAcompanhante(item.getDdd()+item.getTelefone());
		  p.setProfissao(item.getProfissao());
		  p.setEscolaridade(item.getEscolaridade());	
		  p.setRg(item.getIdentidade());
		  p.setOrgaoExpedidor(item.getOrgaoEmissor());
		  p.setEstadoCivil(item.getCivil());
		  p.setNaturalidade(item.getNaturalidade());
		  p.setNacionalidade(item.getNacionalidade());
		  p.setRegistroNascimento(item.getRegistroNascimento());
		  p.setTipoCertidao(item.getTipoCertidao());
		  p.setNomeCartorio(item.getNomeCartorio());
		  p.setLivro(item.getLivro());
		  p.setFolhas(item.getFolhas());
		  p.setTermo(item.getTermo());
		  p.setNumeroDn(item.getNumeroDN());
		  p.setDataEmissao(item.getDataEmissaoCertidao());
		  p.setInternacaoCodigo(item.getInternacaoCodigo());
		  p.setDataInternacao(item.getDataInternacao());
		  p.setDataAltaMedica(item.getDataAltaMedica());
		  p.setDuracaoDias(item.getDuracaoDias());
		  p.setTamCodigo(item.getTamCodigo());
		  p.setTipoAltaMedica(item.getTipoAltaMedica());
		  p.setLto_lto_id(item.getLtoLtoId());
		  p.setLeito(item.getLeito());
		  p.setQuartoNumero(item.getQuartoNumero());
		  p.setCodigoEspecialidade(item.getCodigoEspecialidade());
		  p.setNomeEspecialidade(item.getNomeEspecialidade());
		  p.setNomeReduzidoEspecialidade(item.getNomeReduzidaEspecialidade());
		  p.setSiglaEspecialidade(item.getSiglaEspecialidade());
		  p.setUnfSeq(item.getUnfSeq());
		  p.setUnidadeFuncional(item.getUnidadeFuncional());
		  p.setSiglaUnidadeFuncional(item.getSiglaUnidadeFuncional());
		  p.setAtendimento(String.valueOf(item.getInternacaoCodigo()));
		  p.setCpf(item.getCpf());
          p.setObservacao(item.getObservacao());
          p.setDataAtendimento(item.getDataInternacao());
		  p.setPortaEntrada(item.getEspecialidade());
		  p.setEstabelecimentoSaude("HUB");
		  p.setIndAla(item.getIndAla());
		  p.setAndar(item.getAndar());
		  p.setTciSeq(item.getTciSequencia());
		  p.setCaraterInternacao(item.getCaraterInternacao());
		  p.setMatriculaDigita(item.getMatriculaDigita());
		  p.setVinCodigoDigita(item.getVinCodigoDigita());
		  p.setEfetuaInternacao(item.getNomeSerEfetuaInternacao());
		  p.setMatriculaMedico(item.getMatriculaMedico());
		  p.setCodigoMedico(item.getVinCodigMedico());
		  p.setNomeMedico(item.getNomeMedico());
		  p.setCpfMedico(item.getCpfMedico().intValue());
		  p.setRegConselho(item.getNumeroRegConselho());
		  p.setCidSeq(item.getCidSeq());
		  p.setCidsCodigo(item.getCidsCodigo());
		  p.setCidPrincipal(item.getCidPrincipal());
		  pacienteTolife.add(p);
	  }
	  
	  return pacienteTolife;
  }
  
}
