package br.com.tolife.integrador_hub.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.PostgreSQLDialect;
import org.hibernate.engine.SessionFactoryImplementor;

public class HibernateHubDAO<T> implements InterfaceDAO<T> {
	private Class<T> classe;
	protected Session session;

	private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("emergesProducao");
    private EntityManager em = emf.createEntityManager();

	public HibernateHubDAO(Class<T> classe) {
		super();
		this.classe = classe;
		this.session = (Session) em.getDelegate();	
	}


	public  Dialect getDialectClass() {
		return ((SessionFactoryImplementor) ((Session) em.getDelegate()).getSessionFactory()).getSettings().getDialect();
	}

	public  boolean isBancoPostgres() {
		return (getDialectClass() instanceof PostgreSQLDialect);
	}

	public void atualizar(T bean) {
		session.merge(bean);
		session.flush();
	}


	public void atualizar(Collection<T> beans) {


	}


	public void excluir(T bean) {		
		session.delete(bean);
		session.flush();
	}


	public T getBean(Serializable codigo) {
		T bean = (T)session.get(classe, codigo);
		return bean;
	}


	public List<T> getBeans() {
		Criteria crit = session.createCriteria(classe);
		crit.setCacheable(true);
		List<T> beans = (List<T>)crit.list();
		return beans;
	}



	public List<T> getBeansByExample(T bean) {
		Example example = getExample(bean, MatchMode.EXACT);
		return session.createCriteria(classe).add(example).list();
	}

	public T getUniqueByExample(T bean) {
		Example example = getExample(bean, MatchMode.EXACT);
		return (T) session.createCriteria(classe).add(example).uniqueResult();
	}


	public List<T> getBeansByExampleWithLike(T bean) {
		Example example = getExample(bean, MatchMode.START);
		return session.createCriteria(classe).add(example).list();
	}


	protected Example getExample(T bean, MatchMode matchMode)
	{
		Example example = Example.create(bean);
		example.enableLike(matchMode);
		example.ignoreCase();
		example.excludeZeroes();
		return example;
	}




	public List<T> getBeansByIds(String atributo, List<Serializable> codigos) {
		try{
			if(codigos.size() == 1)
			{
				List<T> resultado = new ArrayList<T>();
				resultado.add(getBean(codigos.get(0)));
				return resultado;
			}else if(codigos.size() > 1)
			{
				Criteria crit = session.createCriteria(classe);
				crit.add(Restrictions.in("codigo", codigos));
				return crit.list();
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return new ArrayList<T>();	

	}



	public T merge(T bean) {

		return (T) session.merge(bean);		
	}


	@SuppressWarnings("unchecked")
	public Long salvar(T bean){
		Long id = (Long) session.save(bean);
		session.flush();
		return id;
	}


	public void salvar(Collection<T> beans) {


	}

	protected Example getExample(T bean)
	{
		Example example = Example.create(bean);
		example.enableLike(MatchMode.START);
		example.ignoreCase();
		example.excludeZeroes();
		return example;
	}


	protected List<T> getBeansAux(Criteria crit, Integer inicio, Integer total)
	{
		crit.setFirstResult(inicio);
		crit.setMaxResults(total);
		return crit.list();
	}

	public List<T> getBeans(Integer inicio, Integer total) {
		Criteria crit = session.createCriteria(classe);		
		return getBeansAux(crit, inicio, total);
	}


	public List<T> getBeansByExample(T bean, Integer inicio, Integer total) {
		Criteria crit = session.createCriteria(classe).add(getExample(bean));		
		return getBeansAux(crit, inicio, total);
	}


	public Integer getTotalRegistros(T bean) {
		Criteria crit = session.createCriteria(classe).setProjection(Projections.rowCount());
		crit.add(getExample(bean));
		return (Integer)crit.uniqueResult();
	}




}
