package br.com.tolife.integrador_hub.dao;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import br.com.tolife.integrador_hub.bean.LogEventos;
import br.com.tolife.integrador_hub.bean.PacienteHub;
import br.com.tolife.integrador_hub.constans.PacienteConstans;

public class HubDAO extends HibernateHubDAO<PacienteHub> {

	LogEventos logEventos = null;
	
	public HubDAO(Class<PacienteHub> classe) {
		super(classe);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<PacienteHub> buscarAtendimento(String dataInternacao) throws Exception{
		
		List listPacientes = new ArrayList<PacienteHub>();
		
		try {
			// Open DBLink
			//System.out.println("Inicializando DBLINK");
			session.createSQLQuery(" SELECT dblink_connect('postgres', 'fdw_dbaghu') ").uniqueResult();
			
			//System.out.println("Rodando consulta que busca proximos pacientes na VIEW da HUB");
			StringBuffer sb = new StringBuffer()
			.append("SELECT p.pac_codigo AS codigoPac,")
			.append("p.prontuario AS prontuario,")
			.append("p.nro_cartao_saude AS sus, ")
			.append("p.nome AS nomePaciente,")
			.append("p.dt_nascimento AS datasinc,")
			.append("p.idade AS idade,")
			.append("p.sexo AS sexo,")
			.append("p.nome_mae AS mae,")
			.append("p.logradouro AS logradouro,")
			.append("p.nro_logradouro AS numero,")
			.append("p.compl_logradouro AS complemento,")
			.append("p.cidade AS cidade,")
			.append("p.uf_sigla AS uf,")
			.append("p.ddd_fone_residencial AS ddd,")
			.append("p.fone_residencial AS telefone,")
			.append("p.cep AS cep,")
			.append("p.profissao AS profissao,")
			.append("p.grau_instrucao AS escolaridade,")
			.append("p.rg AS identidade,")
			.append("p.orgao_emis_rg AS orgaoEmissor,")
			.append("p.estado_civil AS civil,")
			.append("p.naturalidade AS naturalidade,")
			.append("p.nacionalidade AS nacionalidade,")
			.append("p.reg_nascimento AS registroNascimento,")
			.append("p.tipo_certidao AS tipoCertidao,")
			.append("p.nome_cartorio AS nomeCartorio,")
			.append("p.livro AS livro,")
			.append("folhas AS folha,")
			.append("p.termo AS termo,")
			.append("p.data_emissao AS dataEmissao,")
			.append("p.numero_dn AS numeroDN, ")
			.append("i.inter_codigo as internacaoCodigo,") 
			.append("i.dthr_internacao , ")
			.append("i.dthr_alta_medica, ")
			.append("1 as duracao_dias,")
			.append("i.tam_codigo, ")
			.append("i.tipo_alta_medica,") 
			.append("i.lto_lto_id, ")
			.append("i.leito,")
			.append("i.qrt_numero, ")
			.append("i.espec_codigo, ")
			.append("i.nome_especialidade,") 
			.append("i.nome_reduzido_espec,") 
			.append("i.sigla_espec, ")
			.append("i.unf_seq,") 
			.append("i.unidade_funcional,") 
			.append("i.sigla_und_func, ")
			.append("p.criado_em AS dataAtendimento, ")
			.append("p.cor AS raca, ")
			.append("p.nome_pai AS pai, ")
			.append("i.ind_ala,  ")
			.append("i.andar, ")
			.append("i.tci_seq,  ")
			.append("i.carater_internacao, ")
			.append("i.ser_matricula_digita, ")
			.append("i.ser_vin_codigo_digita, ")
			.append("i.nome_ser_efetua_internacao, ")
			.append("i.ser_matricula_medico, ")
			.append("i.ser_vin_codigo_medico, ")
			.append("i.nome_medico, ")
			.append("i.cpf_medico, ")
			.append("i.nro_reg_conselho, ")
			.append("i.cid_seq, ")
			.append("i.cids_codigo, ")
			.append("i.cid_principal ")
			.append("FROM public.vw_dados_internacao_aghu AS i ")
			.append("LEFT JOIN ")
			.append("  (SELECT pac_codigo, ")
			.append("          max(dthr_internacao) AS datah ")
			.append("   FROM public.vw_dados_internacao_aghu ")
			.append("   GROUP BY pac_codigo) AS q ON i.pac_codigo = q.pac_codigo ")
			.append("AND i.dthr_internacao = q.datah ")
			.append("LEFT JOIN public.vw_dados_paciente_aghu p ON p.pac_codigo = i.pac_codigo ")
			.append("WHERE to_char(i.dthr_internacao,'yyyyMMdd HH24:MI:SS')  > :dataInternacao ")
			.append("ORDER BY i.inter_codigo ");
				
			Query query = session.createSQLQuery(sb.toString());
			query.setParameter("dataInternacao", formataDataInternacao(dataInternacao));
					
			listPacientes = query.list();
			//System.out.println("Finalizando consulta que busca proximos pacientes na VIEW da HUB");
		} catch (Exception e) {
			throw e;
		} finally {
			// Disconnect DBLink
			session.createSQLQuery(" SELECT dblink_disconnect('postgres') ").uniqueResult();
			//System.out.println("Finalizando DBLINK");
		}	
		
		System.gc();
		return castList(listPacientes);

	}

	private String formataDataInternacao(String startDateString) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		Date date = (Date)formatter.parse(startDateString);
		SimpleDateFormat newFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		String finalString = newFormat.format(date);
		return finalString;
	}

	private List<PacienteHub> castList(List<Object[]> l) {
		PacienteHub p;

		List<PacienteHub> pacienteHub = new ArrayList<PacienteHub>();
		for (Object[] b : l) {
			p = new PacienteHub();

			System.out.println("[IntegradorHUB] Processado o paciente com o codigo internacao: " + (Integer) b[PacienteConstans.INDEX_INTERNACAO_CODIGO]);
			p.setCodigoPac((Integer) b[PacienteConstans.INDEX_CODIGO_PAC]);
			//System.out.println("[IntegradorHUB] Processado o paciente com o codigo pac: " + (Integer) b[PacienteConstans.INDEX_CODIGO_PAC]);
			p.setDataAtendimento((Date) b[PacienteConstans.INDEX_DATA_ATENDIMENTO]);
			p.setProntuario(Integer
					.toString((Integer) b[PacienteConstans.INDEX_PRONTUARIO]));
			if (((BigInteger) b[PacienteConstans.INDEX_NUMERO_CARTAO_SAUDE] != null)) {
				p.setSus(((BigInteger) b[PacienteConstans.INDEX_NUMERO_CARTAO_SAUDE])
						.toString());
			}
			p.setNomePaciente((String) b[PacienteConstans.INDEX_NOME_PACIENTE]);
			p.setRaca((String) b[PacienteConstans.INDEX_RACA]);
			p.setDataNascimento((Date) b[PacienteConstans.INDEX_DATA_NASCIMENTO]);
			p.setIdade(((Double) b[PacienteConstans.INDEX_IDADE]).intValue());
			p.setSexo((String) b[PacienteConstans.INDEX_SEXO]);
			p.setMae((String) b[PacienteConstans.INDEX_MAE_NOME]);
			p.setPai((String) b[PacienteConstans.INDEX_MAE_PAI]);
			p.setLogradouro((String) b[PacienteConstans.INDEX_LOGRADOURO]);
			p.setNumero((String) b[PacienteConstans.INDEX_NR_LOGRADOURO]);
			p.setComplemento((String) b[PacienteConstans.INDEX_NR_LOGRADOURO_COMPLEMENTO]);
			p.setCidade((String) b[PacienteConstans.INDEX_CIDADE]);
			p.setUf((String) b[PacienteConstans.INDEX_UF]);
			if ((Integer) b[PacienteConstans.INDEX_CEP] != null) {
				p.setCep(Integer.toString((Integer) b[PacienteConstans.INDEX_CEP]));	
			}
			if (((BigInteger) b[PacienteConstans.INDEX_TELEFONE] != null)) {
				p.setTelefone(((BigInteger) b[PacienteConstans.INDEX_TELEFONE])
						.toString());
				p.setDdd(Short
						.toString((Short) b[PacienteConstans.INDEX_DDD_TELEFONE]));
			}
			p.setProfissao((String) b[PacienteConstans.INDEX_PROFISSAO]);
			
			if ((Short) b[PacienteConstans.INDEX_ESCOLARIDADE] != null) {
				p.setEscolaridade(Short
						.toString((Short) b[PacienteConstans.INDEX_ESCOLARIDADE]));
			}
			p.setIdentidade((String) b[PacienteConstans.INDEX_IDENTIDADE]);
			p.setOrgaoEmissor((String) b[PacienteConstans.INDEX_ORGAO_EMISSOR]);
			p.setCivil((String) b[PacienteConstans.INDEX_ESTADO_CIVIL]);
			p.setNaturalidade((String) b[PacienteConstans.INDEX_NATURALIDADE]);
			p.setNacionalidade((String) b[PacienteConstans.INDEX_NACIONALIDADE]);
			p.setRegistroNascimento((String) b[PacienteConstans.INDEX_REGISTRO_NASCIMENTO]);
			p.setTipoCertidao((Short) b[PacienteConstans.INDEX_TIPO_CERTIDAO]);
			p.setNomeCartorio((String) b[PacienteConstans.INDEX_NOME_CARTORIO]);
			p.setLivro((String) b[PacienteConstans.INDEX_LIVRO]);
			p.setFolhas((Short) b[PacienteConstans.INDEX_FOLHAS]);
			p.setTermo((Integer) b[PacienteConstans.INDEX_TERMO]);
			p.setDataEmissaoCertidao((Date) b[PacienteConstans.INDEX_DATA_EMISSAO_CERTIDAO]);
			p.setNumeroDN((Integer) b[PacienteConstans.INDEX_NUMERO_DN]);
			p.setInternacaoCodigo((Integer) b[PacienteConstans.INDEX_INTERNACAO_CODIGO]);
			p.setDataInternacao((Date) b[PacienteConstans.INDEX_DATA_INTERNACAO]);
			p.setDataAltaMedica((Date) b[PacienteConstans.INDEX_DATA_ALTA_MEDICA]);
			p.setDuracaoDias((Integer) b[PacienteConstans.INDEX_DURACAO_DIAS]);
			p.setTamCodigo((String) b[PacienteConstans.INDEX_TAM_CODIGO]);
			p.setTipoAltaMedica((String) b[PacienteConstans.INDEX_TIPO_ALTA_MEDICA]);
			p.setLtoLtoId((String) b[PacienteConstans.INDEX_LTO_LTO_ID]);
			p.setLeito((String) b[PacienteConstans.INDEX_LEITO]);
			p.setQuartoNumero((Short) b[PacienteConstans.INDEX_QUARTO_NUMERO]);
			p.setCodigoEspecialidade((Short) b[PacienteConstans.INDEX_CODIGO_ESPECIALIDADE]);
			p.setNomeEspecialidade((String) b[PacienteConstans.INDEX_NOME_ESPECIALIDADE]);
			p.setNomeReduzidaEspecialidade((String) b[PacienteConstans.INDEX_NOME_REDUZIDO_ESPECIALIDADE]);
			p.setSiglaEspecialidade((String) b[PacienteConstans.INDEX_SIGLA_ESPECIALIDADE]);
			p.setUnfSeq((Short) b[PacienteConstans.INDEX_UNF_SEQ]);
			p.setUnidadeFuncional((String) b[PacienteConstans.INDEX_UNIDADE_FUNCIONAL]);
			p.setSiglaUnidadeFuncional((String) b[PacienteConstans.INDEX_SIGLA_UNIDADE_NACIONAL]);
			p.setIndAla((String) b[PacienteConstans.INDEX_IND_ALA]);
			p.setAndar((String) b[PacienteConstans.INDEX_ANDAR]);
			p.setTciSequencia((Short) b[PacienteConstans.INDEX_TCI_SEQ]);
			p.setCaraterInternacao((String) b[PacienteConstans.INDEX_CARATER_INTERNACAO]);
			p.setMatriculaDigita((Integer) b[PacienteConstans.INDEX_MATRICULA_DIGITA]);
			p.setVinCodigoDigita((Short) b[PacienteConstans.INDEX_VIN_CODIGO_DIGITA]);
			p.setNomeSerEfetuaInternacao((String) b[PacienteConstans.INDEX_NOME_SER_EFETUA_INTERNACAO]);
			p.setMatriculaMedico((Integer) b[PacienteConstans.INDEX_MATRICULA_MEDICO]);
			p.setVinCodigMedico((Short) b[PacienteConstans.INDEX_VIN_CODIGO_MEDICO]);
			p.setNomeMedico((String) b[PacienteConstans.INDEX_NOME_MEDICO]);
			p.setCpfMedico((BigInteger) b[PacienteConstans.INDEX_CPF_MEDICO]);
			p.setNumeroRegConselho((String) b[PacienteConstans.INDEX_NUMERO_REG_CONSELHO]);
			p.setCidSeq((Integer) b[PacienteConstans.INDEX_CID_SEQ]);
			p.setCidsCodigo((String) b[PacienteConstans.INDEX_CIDS_CODIGO]);
			p.setCidPrincipal((String) b[PacienteConstans.INDEX_CID_PRINCIPAL]);

			pacienteHub.add(p);
			// logEventos = new LogEventos("INTEGRACAO HUB",
			// "INSERINDO DADOS DO PACIENTE NA TABELA INTEGRACAO_PACIENTE",
			// p.getCodigoPac().toString());

		}

		return pacienteHub;
	}

}
