package br.com.tolife.integrador_hub.dao;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.Hibernate;
import org.hibernate.Query;

import br.com.tolife.integrador_hub.bean.IntegracaoPaciente;

public class IntegracaoPacienteDAO extends HibernateEmergesDAO<IntegracaoPaciente>{

	public IntegracaoPacienteDAO(Class<IntegracaoPaciente> classe) {
		super(classe);
	}
	
	public void openDbLink(){
			StringBuffer sb = new StringBuffer();
			sb.append(" SELECT dblink_connect('postgres', 'fdw_dbaghu') ");  
			Query query = session.createSQLQuery(sb.toString());
			query.uniqueResult();
	}
	
	@SuppressWarnings("deprecation")
	public String obterUltimaDataHoraInternacao(){
		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT valor FROM parametro WHERE chave = 'PARAMETRO_ULTIMO_ATENDIMENTO_HUB' ");  
		Query query = session.createSQLQuery(sb.toString()).addScalar("valor",Hibernate.STRING);
		String res = (String) query.uniqueResult();
		//System.out.println("Obtendo a ultima data/hora da Internacacao na tabela Parametro:" + res);
		return res ;		
	}
	
	@SuppressWarnings("deprecation")
	public Date obterMaxDataHoraInternacao(){
			StringBuffer sb = new StringBuffer();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			sb.append(" SELECT MAX(dthr_internacao) AS valor FROM integracao_paciente ");  
			Query query = session.createSQLQuery(sb.toString()).addScalar("valor",Hibernate.TIMESTAMP);
			Date resultado = (Date) query.uniqueResult();
			System.out.println("[IntegradorHUB] Maior data/hora da Internacacao(tabela integracao_paciente): " + resultado);
			return (Date) resultado;
	}
	
	public void atualizarUltimaDataHoraParamento(Date valor){
		//System.out.println("Atualizando ultimo parametro atendimento. Valor a ser atualizadado na tabela Parametro: "+ valor);
		StringBuffer sb = new StringBuffer();
		sb.append(" UPDATE parametro SET valor =:valor WHERE chave = 'PARAMETRO_ULTIMO_ATENDIMENTO_HUB'  ");  
		Query query = session.createSQLQuery(sb.toString());
		query.setParameter("valor", valor);
		query.executeUpdate();
	}

	public void closeDbLink() {
		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT dblink_disconnect('postgres') ");  
		Query query = session.createSQLQuery(sb.toString());
		query.executeUpdate();
		
	}
}
