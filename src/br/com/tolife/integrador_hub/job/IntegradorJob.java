
package br.com.tolife.integrador_hub.job;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.tolife.integrador_hub.facade.FacadeIntegracao;


/**
 *
 * @author Hernandes Santos
 */
public class IntegradorJob implements Job {

    FacadeIntegracao facade = new FacadeIntegracao();
	Logger logger = Logger.getLogger("LOG");
	//LogEventos logEventos = null;  
    
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {

    	//logEventos = new LogEventos("INTEGRACAO HUB", "Inicialização da Integração", "");
    	//facade.salvarLog(logEventos);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try{
        	
        	System.out.println("[IntegradorHUB] Data/hora--> "+df.format(new Date()));
        	facade.integrar();
        }catch(Exception e){
        	//logEventos = new LogEventos("INTEGRACAO HUB", "HOUVE UM ERRO NO PROCESSAMENTO DA INTEGRACAO HUB"+ e.getCause() != null ? e.getCause().getMessage() : e.getMessage(), "");
        	//facade.salvarLog(logEventos);
        	System.out.println("****************************************************************************************************");
        	System.out.println(" HOUVE UM ERRO NO PROCESSAMENTO DO JOB DE INTEGRACAO HUB ("+df.format(new Date())+")");
        	System.out.println("****ERRO**** ["+e.getMessage()+"]"+" mais detalhes no arquivo catalina");
        	e.printStackTrace();
        	System.out.println("****************************************************************************************************");
        }
    }
}