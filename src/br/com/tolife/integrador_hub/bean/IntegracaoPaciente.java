package br.com.tolife.integrador_hub.bean;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Hernandes Santos
 */
@Entity
@Table(name = "integracao_paciente")
@SequenceGenerator(name = "integracao_paciente_id_seq", sequenceName = "integracao_paciente_id_seq")
public class IntegracaoPaciente implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "integracao_paciente_id_seq")
    @Column(name = "ID_INTEGRACAO")
    private Long idIntegracao;
    
    @Column(name = "NOME")
    private String nome;
    
    @Column(name = "DATA_NASCIMENTO")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;
    
    @Column(name = "IDADE")
    private Integer idade;
    
    @Column(name = "SEXO")
    private String sexo;
    
    @Column(name = "NOME_MAE")
    private String nomeMae;
    
    @Column(name = "NOME_PAI")
    private String nomePai;
    
    @Column(name = "NATURALIDADE")
    private String naturalidade;
    
    @Column(name = "NACIONALIDADE")
    private String nacionalidade;
    
    @Column(name = "PROFISSAO")
    private String profissao;
    
    @Column(name = "RG")
    private String rg;
    
    @Column(name = "ORGAO_EXPEDIDOR")
    private String orgaoExpedidor;
    
    @Column(name = "CPF")
    private String cpf;
    
    @Column(name = "CARTAO_SUS")
    private String cartaoSUS;
    
    @Column(name = "PRONTUARIO")
    private String prontuario;
    
    @Column(name = "LOGRADOURO")
    private String logradouro;
    
    @Column(name = "NUMERO")
    private Integer numero;
    
    @Column(name = "COMPLEMENTO")
    private String complemento;
    
    @Column(name = "BAIRRO")
    private String bairro;
    
    @Column(name = "CIDADE")
    private String cidade;
    
    @Column(name = "ESTADO")
    private String estado;
    
    @Column(name = "CEP")
    private String cep;
    
    @Column(name = "ESCOLARIDADE")
    private String escolaridade;
    
    @Column(name = "ESTADO_CIVIL")
    private String estadoCivil;
    
    @Column(name = "RACA")
    private String raca;
    
    @Column(name = "ORIGEM")
    private String origem;
    
    @Column(name = "TIPO_CHEGADA")
    private String tipoChegada;
    
    @Column(name = "NECESSIDADES_PACIENTE")
    private String necessidadePaciente;
    
    @Column(name = "OBSERVACAO")
    private String observacao;
    
    @Column(name = "PERTENCES")
    private String pertences;
    
    @Column(name = "PLANO_SAUDE")
    private String planoSaude;
    
    @Column(name = "NUMERO_PLANO")
    private Integer numeroPlano;
    
    @Column(name = "VALIDADE")
    @Temporal(TemporalType.DATE)
    private Date dataValidadePlano;
    
    @Column(name = "NOME_ACOMPANHANTE")
    private String nomeAcompanhante;
    
    @Column(name = "TELEFONE_ACOMPANHANTE")
    private String telefoneAcompanhante;
    
    @Column(name = "CELULAR_ACOMPANHANTE")
    private String celularAcompanhante;
    
    @Column(name = "PARENTESCO")
    private String parentesco;
    
    @Column(name = "LOGRADOURO_ACOMPANHANTE")
    private String logradouroAcompanhante;
    
    @Column(name = "NUMERO_ACOMPANHANTE")
    private Integer numeroResidenciaAcompanhante;
    
    @Column(name = "COMPLEMENTO_ACOMPANHANTE")
    private String complementoAcompanhante;
    
    @Column(name = "BAIRRO_ACOMPANHANTE")
    private String bairroAcompanhante;
    
    @Column(name = "CIDADE_ACOMPANHANTE")
    private String cidadeAcompanhante;
    
    @Column(name = "ESTADO_ACOMPANHANTE")
    private String estadoAcompanhante;
    
    @Column(name = "CEP_ACOMPANHANTE")
    private String cepAcompanhante;
    
    @Column(name = "SENHA")
    private String senha;
    
    @Column(name = "ATENDIMENTO")
    private String atendimento;
    
    @Column(name = "DATA_ATENDIMENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAtendimento;
    
    @Column(name = "CLASSIFICADO")
    private String classificado;
    
    @Column(name = "ID_PACIENTE")
    private String idPaciente;
    
    @Column(name = "PORTA_ENTRADA")
    private String portaEntrada;
    
    @Column(name = "ESTABELECIMENTO_SAUDE")
    private String estabelecimentoSaude;
    
    @Column(name = "PRIORITARIO")
    private String prioritario;
    
    @Column(name = "NOME_RECEPCIONISTA")
    private String nomeRecepcionista;

    @Column(name = "REG_NASCIMENTO")
    private String registroNascimento;
    
    @Column(name = "TIPO_CERTIDAO")
    private Short tipoCertidao;
    
    @Column(name = "NOME_CARTORIO")
    private String nomeCartorio;
    
    @Column(name = "LIVRO")
    private String livro;
    
    @Column(name = "FOLHAS")
    private Short folhas;
    
    @Column(name = "TERMO")
    private Integer termo;
    
    @Column(name = "NUMERO_DN")
    private Integer numeroDn;
    
    @Column(name = "DATA_EMISSAO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataEmissao;
        
    @Column(name = "INTER_CODIGO")
    private Integer internacaoCodigo;

    @Column(name = "DTHR_INTERNACAO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInternacao;

    @Column(name = "DTHR_ALTA_MEDICA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataAltaMedica;
    
    @Column(name = "DURACAO_DIAS")
    private Integer duracaoDias;
    
    @Column(name = "TAM_CODIGO")
    private String tamCodigo;
    
    @Column(name = "TIPO_ALTA_MEDICA")
    private String tipoAltaMedica;
    
    @Column(name = "LTO_LTO_ID")
    private String lto_lto_id;
    
    @Column(name = "LEITO")
    private String leito;
    
    @Column(name = "QRT_NUMERO")
    private Short quartoNumero;

    @Column(name = "ESPEC_CODIGO")
    private Short codigoEspecialidade;
    
    @Column(name = "NOME_ESPECIALIDADE")
    private String nomeEspecialidade;
    
    @Column(name = "NOME_REDUZIDO_ESPEC")
    private String nomeReduzidoEspecialidade;
    
    @Column(name = "SIGLA_ESPEC")
    private String siglaEspecialidade;
    
    @Column(name = "UNF_SEQ")
    private Short unfSeq;
    
    @Column(name = "UNIDADE_FUNCIONAL")
    private String unidadeFuncional;
    
    @Column(name = "SIGLA_UND_FUNC")
    private String siglaUnidadeFuncional;

    @Column(name = "ind_ala")
    private String indAla;
    
    @Column(name = "andar")
    private String andar;
    
    @Column(name = "tci_seq")
    private Short tciSeq;
    
    @Column(name = "carater_internacao")
    private String caraterInternacao;
    
    @Column(name = "ser_matricula_digita")
    private Integer matriculaDigita;
    
    @Column(name = "ser_vin_codigo_digita")
    private Short vinCodigoDigita;
    
    @Column(name = "nome_ser_efetua_internacao")
    private String efetuaInternacao;
    
    @Column(name = "ser_matricula_medico")
    private Integer matriculaMedico;
    
    @Column(name = "ser_vin_codigo_medico")
    private Short codigoMedico;
    
    @Column(name = "nome_medico")
    private String nomeMedico;
    
    @Column(name = "cpf_medico")
    private Integer cpfMedico;
    
    @Column(name = "nro_reg_conselho")
    private String regConselho;
    
    @Column(name = "cid_seq")
    private Integer cidSeq;
    
    @Column(name = "cids_codigo")
    private String cidsCodigo;
    
	@Column(name = "cid_principal")
    private String cidPrincipal;
    
    public String getIndAla() {
		return indAla;
	}

	public void setIndAla(String indAla) {
		this.indAla = indAla;
	}

	public String getAndar() {
		return andar;
	}

	public void setAndar(String andar) {
		this.andar = andar;
	}

	public Short getTciSeq() {
		return tciSeq;
	}

	public void setTciSeq(Short tciSeq) {
		this.tciSeq = tciSeq;
	}

	public String getCaraterInternacao() {
		return caraterInternacao;
	}

	public void setCaraterInternacao(String caraterInternacao) {
		this.caraterInternacao = caraterInternacao;
	}

	public Integer getMatriculaDigita() {
		return matriculaDigita;
	}

	public void setMatriculaDigita(Integer matriculaDigita) {
		this.matriculaDigita = matriculaDigita;
	}

	public String getEfetuaInternacao() {
		return efetuaInternacao;
	}

	public void setEfetuaInternacao(String efetuaInternacao) {
		this.efetuaInternacao = efetuaInternacao;
	}

	public Integer getMatriculaMedico() {
		return matriculaMedico;
	}

	public void setMatriculaMedico(Integer matriculaMedico) {
		this.matriculaMedico = matriculaMedico;
	}

	public Short getCodigoMedico() {
		return codigoMedico;
	}

	public void setCodigoMedico(Short codigoMedico) {
		this.codigoMedico = codigoMedico;
	}

	public String getNomeMedico() {
		return nomeMedico;
	}

	public void setNomeMedico(String nomeMedico) {
		this.nomeMedico = nomeMedico;
	}

	public Integer getCpfMedico() {
		return cpfMedico;
	}

	public void setCpfMedico(Integer cpfMedico) {
		this.cpfMedico = cpfMedico;
	}

	public String getRegConselho() {
		return regConselho;
	}

	public void setRegConselho(String regConselho) {
		this.regConselho = regConselho;
	}

	public Integer getCidSeq() {
		return cidSeq;
	}

	public void setCidSeq(Integer cidSeq) {
		this.cidSeq = cidSeq;
	}

	public String getCidsCodigo() {
		return cidsCodigo;
	}

	public void setCidsCodigo(String cidsCodigo) {
		this.cidsCodigo = cidsCodigo;
	}

	public String getCidPrincipal() {
		return cidPrincipal;
	}

	public void setCidPrincipal(String cidPrincipal) {
		this.cidPrincipal = cidPrincipal;
	}

    public String getNomeRecepcionista() {
        return nomeRecepcionista;
    }

    public void setNomeRecepcionista(String nomeRecepcionista) {
        this.nomeRecepcionista = nomeRecepcionista;
    }

    public Long getIdIntegracao() {
        return idIntegracao;
    }

    public void setIdIntegracao(Long idIntegracao) {
        this.idIntegracao = idIntegracao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    public String getNomePai() {
        return nomePai;
    }

    public void setNomePai(String nomePai) {
        this.nomePai = nomePai;
    }

    public String getNaturalidade() {
        return naturalidade;
    }

    public void setNaturalidade(String naturalidade) {
        this.naturalidade = naturalidade;
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getOrgaoExpedidor() {
        return orgaoExpedidor;
    }

    public void setOrgaoExpedidor(String orgaoExpedidor) {
        this.orgaoExpedidor = orgaoExpedidor;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCartaoSUS() {
        return cartaoSUS;
    }

    public void setCartaoSUS(String cartaoSUS) {
        this.cartaoSUS = cartaoSUS;
    }

    public String getProntuario() {
        return prontuario;
    }

    public void setProntuario(String prontuario) {
        this.prontuario = prontuario;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getEscolaridade() {
        return escolaridade;
    }

    public void setEscolaridade(String escolaridade) {
        this.escolaridade = escolaridade;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getTipoChegada() {
        return tipoChegada;
    }

    public void setTipoChegada(String tipoChegada) {
        this.tipoChegada = tipoChegada;
    }

    public String getNecessidadePaciente() {
        return necessidadePaciente;
    }

    public void setNecessidadePaciente(String necessidadePaciente) {
        this.necessidadePaciente = necessidadePaciente;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getPertences() {
        return pertences;
    }

    public void setPertences(String pertences) {
        this.pertences = pertences;
    }

    public String getPlanoSaude() {
        return planoSaude;
    }

    public void setPlanoSaude(String planoSaude) {
        this.planoSaude = planoSaude;
    }

    public Integer getNumeroPlano() {
        return numeroPlano;
    }

    public void setNumeroPlano(Integer numeroPlano) {
        this.numeroPlano = numeroPlano;
    }

    public Date getDataValidadePlano() {
        return dataValidadePlano;
    }

    public void setDataValidadePlano(Date dataValidadePlano) {
        this.dataValidadePlano = dataValidadePlano;
    }

    public String getNomeAcompanhante() {
        return nomeAcompanhante;
    }

    public void setNomeAcompanhante(String nomeAcompanhante) {
        this.nomeAcompanhante = nomeAcompanhante;
    }

    public String getTelefoneAcompanhante() {
        return telefoneAcompanhante;
    }

    public void setTelefoneAcompanhante(String telefoneAcompanhante) {
        this.telefoneAcompanhante = telefoneAcompanhante;
    }

    public String getCelularAcompanhante() {
        return celularAcompanhante;
    }

    public void setCelularAcompanhante(String celularAcompanhante) {
        this.celularAcompanhante = celularAcompanhante;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getLogradouroAcompanhante() {
        return logradouroAcompanhante;
    }

    public void setLogradouroAcompanhante(String logradouroAcompanhante) {
        this.logradouroAcompanhante = logradouroAcompanhante;
    }

    public Integer getNumeroResidenciaAcompanhante() {
        return numeroResidenciaAcompanhante;
    }

    public void setNumeroResidenciaAcompanhante(Integer numeroResidenciaAcompanhante) {
        this.numeroResidenciaAcompanhante = numeroResidenciaAcompanhante;
    }

    public String getComplementoAcompanhante() {
        return complementoAcompanhante;
    }

    public void setComplementoAcompanhante(String complementoAcompanhante) {
        this.complementoAcompanhante = complementoAcompanhante;
    }

    public String getBairroAcompanhante() {
        return bairroAcompanhante;
    }

    public void setBairroAcompanhante(String bairroAcompanhante) {
        this.bairroAcompanhante = bairroAcompanhante;
    }

    public String getCidadeAcompanhante() {
        return cidadeAcompanhante;
    }

    public void setCidadeAcompanhante(String cidadeAcompanhante) {
        this.cidadeAcompanhante = cidadeAcompanhante;
    }

    public String getEstadoAcompanhante() {
        return estadoAcompanhante;
    }

    public void setEstadoAcompanhante(String estadoAcompanhante) {
        this.estadoAcompanhante = estadoAcompanhante;
    }

    public String getCepAcompanhante() {
        return cepAcompanhante;
    }

    public void setCepAcompanhante(String cepAcompanhante) {
        this.cepAcompanhante = cepAcompanhante;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getAtendimento() {
        return atendimento;
    }

    public void setAtendimento(String atendimento) {
        this.atendimento = atendimento;
    }

    public Date getDataAtendimento() {
        return dataAtendimento;
    }

    public void setDataAtendimento(Date dataAtendimento) {
        this.dataAtendimento = dataAtendimento;
    }

    public String getClassificado() {
        return classificado;
    }

    public void setClassificado(String classificado) {
        this.classificado = classificado;
    }

    public String getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(String idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getPortaEntrada() {
        return portaEntrada;
    }

    public void setPortaEntrada(String portaEntrada) {
        this.portaEntrada = portaEntrada;
    }

    public String getEstabelecimentoSaude() {
        return estabelecimentoSaude;
    }

    public void setEstabelecimentoSaude(String estabelecimentoSaude) {
        this.estabelecimentoSaude = estabelecimentoSaude;
    }

    public String getPrioritario() {
        return prioritario;
    }

    public void setPrioritario(String prioritario) {
        this.prioritario = prioritario;
    }

	public Integer getInternacaoCodigo() {
		return internacaoCodigo;
	}

	public void setInternacaoCodigo(Integer internacaoCodigo) {
		this.internacaoCodigo = internacaoCodigo;
	}

	public Date getDataInternacao() {
		return dataInternacao;
	}

	public void setDataInternacao(Date dataInternacao) {
		this.dataInternacao = dataInternacao;
	}

	public Date getDataAltaMedica() {
		return dataAltaMedica;
	}

	public void setDataAltaMedica(Date dataAltaMedica) {
		this.dataAltaMedica = dataAltaMedica;
	}

	public Integer getDuracaoDias() {
		return duracaoDias;
	}

	public void setDuracaoDias(Integer duracaoDias) {
		this.duracaoDias = duracaoDias;
	}

	public String getTamCodigo() {
		return tamCodigo;
	}

	public void setTamCodigo(String tamCodigo) {
		this.tamCodigo = tamCodigo;
	}

	public String getTipoAltaMedica() {
		return tipoAltaMedica;
	}

	public void setTipoAltaMedica(String tipoAltaMedica) {
		this.tipoAltaMedica = tipoAltaMedica;
	}

	public String getLto_lto_id() {
		return lto_lto_id;
	}

	public void setLto_lto_id(String lto_lto_id) {
		this.lto_lto_id = lto_lto_id;
	}

	public String getLeito() {
		return leito;
	}

	public void setLeito(String leito) {
		this.leito = leito;
	}

	public String getNomeEspecialidade() {
		return nomeEspecialidade;
	}

	public void setNomeEspecialidade(String nomeEspecialidade) {
		this.nomeEspecialidade = nomeEspecialidade;
	}

	public String getNomeReduzidoEspecialidade() {
		return nomeReduzidoEspecialidade;
	}

	public void setNomeReduzidoEspecialidade(String nomeReduzidoEspecialidade) {
		this.nomeReduzidoEspecialidade = nomeReduzidoEspecialidade;
	}

	public String getSiglaEspecialidade() {
		return siglaEspecialidade;
	}

	public void setSiglaEspecialidade(String siglaEspecialidade) {
		this.siglaEspecialidade = siglaEspecialidade;
	}

	public String getUnidadeFuncional() {
		return unidadeFuncional;
	}

	public void setUnidadeFuncional(String unidadeFuncional) {
		this.unidadeFuncional = unidadeFuncional;
	}

	public String getSiglaUnidadeFuncional() {
		return siglaUnidadeFuncional;
	}

	public void setSiglaUnidadeFuncional(String siglaUnidadeFuncional) {
		this.siglaUnidadeFuncional = siglaUnidadeFuncional;
	}

	public Short getUnfSeq() {
		return unfSeq;
	}

	public void setUnfSeq(Short unfSeq) {
		this.unfSeq = unfSeq;
	}

	public Short getCodigoEspecialidade() {
		return codigoEspecialidade;
	}

	public void setCodigoEspecialidade(Short codigoEspecialidade) {
		this.codigoEspecialidade = codigoEspecialidade;
	}

	public Short getQuartoNumero() {
		return quartoNumero;
	}

	public void setQuartoNumero(Short quartoNumero) {
		this.quartoNumero = quartoNumero;
	}

	public String getRegistroNascimento() {
		return registroNascimento;
	}

	public void setRegistroNascimento(String registroNascimento) {
		this.registroNascimento = registroNascimento;
	}

	public Short getTipoCertidao() {
		return tipoCertidao;
	}

	public void setTipoCertidao(Short tipoCertidao) {
		this.tipoCertidao = tipoCertidao;
	}

	public String getNomeCartorio() {
		return nomeCartorio;
	}

	public void setNomeCartorio(String nomeCartorio) {
		this.nomeCartorio = nomeCartorio;
	}

	public String getLivro() {
		return livro;
	}

	public void setLivro(String livro) {
		this.livro = livro;
	}

	public Integer getTermo() {
		return termo;
	}

	public void setTermo(Integer termo) {
		this.termo = termo;
	}

	public Short getFolhas() {
		return folhas;
	}

	public void setFolhas(Short folhas) {
		this.folhas = folhas;
	}

	public Integer getNumeroDn() {
		return numeroDn;
	}

	public void setNumeroDn(Integer numeroDn) {
		this.numeroDn = numeroDn;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Short getVinCodigoDigita() {
		return vinCodigoDigita;
	}

	public void setVinCodigoDigita(Short vinCodigoDigita) {
		this.vinCodigoDigita = vinCodigoDigita;
	}

}

