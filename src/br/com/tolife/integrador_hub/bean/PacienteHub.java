package br.com.tolife.integrador_hub.bean;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class PacienteHub {
	
	private Integer codigoPac;
	
	private String nomePaciente;
	
	private String identidade;
	
	private Date dataNascimento;
	
	private String trabalho;
	
	private String naturalidade; 
	
	private String pai;
	
	private String mae;
	
	private String sexo;
	
	private String cpf; 	 
	
	private String logradouro;
	
	private String cep;	 	 
	
	private String bairro; 
	
	private String cidade;    
	
	private String uf;		 
	
	private String ddd;
	
	private String telefone;
	
	private String profissao;
	
	private String orgaoEmissor;
	
	private String civil;     
	
	private String observacao; 
	
	private String sus; 		 
	
	private String plano;	 
	
	private Date validade ; 
	
	private String numero;	  
	
	private String complemento; 
	
	private String escolaridade; 
	
	private String prontuario ;  
	
	private String nacionalidade;
	
	private String registroNascimento;
	
	private Short tipoCertidao;
	
	private String nomeCartorio;
	
	private String livro;
	
	private Short folhas;
	
	private Integer termo;
	
	private Date dataEmissaoCertidao;
	
	private Integer numeroDN;
	
	private Integer idade;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datasinc;
	
	private String especialidade;

	private Integer internacaoCodigo;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInternacao;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAltaMedica;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAtendimento;
		
	private Integer duracaoDias;
	
	private String tamCodigo;
	
	private String tipoAltaMedica ;
	
	private String ltoLtoId;
	
	private String leito;
	
	private Short quartoNumero;
	
	private Short codigoEspecialidade;
	
	private String nomeEspecialidade;
	
	private String nomeReduzidaEspecialidade;
	
	private String siglaEspecialidade;
	
	private Short unfSeq;
	
	private String unidadeFuncional;
	
	private String siglaUnidadeFuncional;
	
	private String raca;
	
	private String indAla;
	
	private String andar;
	
	private Short tciSequencia;
	
	private String caraterInternacao;
	
	private Integer matriculaDigita;
	
	private Short vinCodigoDigita;
	
	private String nomeSerEfetuaInternacao;
	
	private Integer matriculaMedico;
	
	private Short vinCodigMedico;
	
	private String nomeMedico;
	
	private BigInteger cpfMedico;
	
	private String numeroRegConselho;
	
	private Integer cidSeq;
	
	private String cidsCodigo;
	
	private String cidPrincipal;
	
	
	public String getNomePaciente() {
		return nomePaciente;
	}

	public void setNomePaciente(String nomePaciente) {
		this.nomePaciente = nomePaciente;
	}

	public String getIdentidade() {
		return identidade;
	}

	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getTrabalho() {
		return trabalho;
	}

	public void setTrabalho(String trabalho) {
		this.trabalho = trabalho;
	}

	public String getNaturalidade() {
		return naturalidade;
	}

	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCivil() {
		return civil;
	}

	public void setCivil(String civil) {
		this.civil = civil;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getPlano() {
		return plano;
	}

	public void setPlano(String plano) {
		this.plano = plano;
	}

	public Date getValidade() {
		return validade;
	}

	public void setValidade(Date validade) {
		this.validade = validade;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getProntuario() {
		return prontuario;
	}

	public void setProntuario(String prontuario) {
		this.prontuario = prontuario;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	

	public Date getDatasinc() {
		return datasinc;
	}

	public void setDatasinc(Date datasinc) {
		this.datasinc = datasinc;
	}

	public Integer getCodigoPac() {
		return codigoPac;
	}

	public void setCodigoPac(Integer codigoPac) {
		this.codigoPac = codigoPac;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public String getOrgaoEmissor() {
		return orgaoEmissor;
	}

	public void setOrgaoEmissor(String orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}

	public String getRegistroNascimento() {
		return registroNascimento;
	}

	public String getNomeCartorio() {
		return nomeCartorio;
	}

	public void setNomeCartorio(String nomeCartorio) {
		this.nomeCartorio = nomeCartorio;
	}

	public String getLivro() {
		return livro;
	}

	public void setLivro(String livro) {
		this.livro = livro;
	}

	public Date getDataEmissaoCertidao() {
		return dataEmissaoCertidao;
	}

	public void setDataEmissaoCertidao(Date dataEmissaoCertidao) {
		this.dataEmissaoCertidao = dataEmissaoCertidao;
	}

	public String getSus() {
		return sus;
	}

	public void setSus(String sus) {
		this.sus = sus;
	}

	public Integer getInternacaoCodigo() {
		return internacaoCodigo;
	}

	public void setInternacaoCodigo(Integer internacaoCodigo) {
		this.internacaoCodigo = internacaoCodigo;
	}

	public Date getDataInternacao() {
		return dataInternacao;
	}

	public void setDataInternacao(Date dataInternacao) {
		this.dataInternacao = dataInternacao;
	}

	public Date getDataAltaMedica() {
		return dataAltaMedica;
	}

	public void setDataAltaMedica(Date dataAltaMedica) {
		this.dataAltaMedica = dataAltaMedica;
	}

	public Integer getDuracaoDias() {
		return duracaoDias;
	}

	public void setDuracaoDias(Integer duracaoDias) {
		this.duracaoDias = duracaoDias;
	}

	public String getTamCodigo() {
		return tamCodigo;
	}

	public void setTamCodigo(String tamCodigo) {
		this.tamCodigo = tamCodigo;
	}

	public String getTipoAltaMedica() {
		return tipoAltaMedica;
	}

	public void setTipoAltaMedica(String tipoAltaMedica) {
		this.tipoAltaMedica = tipoAltaMedica;
	}

	public String getLtoLtoId() {
		return ltoLtoId;
	}

	public void setLtoLtoId(String ltoLtoId) {
		this.ltoLtoId = ltoLtoId;
	}

	public String getLeito() {
		return leito;
	}

	public void setLeito(String leito) {
		this.leito = leito;
	}

	public String getNomeEspecialidade() {
		return nomeEspecialidade;
	}

	public void setNomeEspecialidade(String nomeEspecialidade) {
		this.nomeEspecialidade = nomeEspecialidade;
	}

	public String getNomeReduzidaEspecialidade() {
		return nomeReduzidaEspecialidade;
	}

	public void setNomeReduzidaEspecialidade(String nomeReduzidaEspecialidade) {
		this.nomeReduzidaEspecialidade = nomeReduzidaEspecialidade;
	}

	public String getSiglaEspecialidade() {
		return siglaEspecialidade;
	}

	public void setSiglaEspecialidade(String siglaEspecialidade) {
		this.siglaEspecialidade = siglaEspecialidade;
	}


	public String getUnidadeFuncional() {
		return unidadeFuncional;
	}

	public void setUnidadeFuncional(String unidadeFuncional) {
		this.unidadeFuncional = unidadeFuncional;
	}

	public String getSiglaUnidadeFuncional() {
		return siglaUnidadeFuncional;
	}

	public void setSiglaUnidadeFuncional(String siglaUnidadeFuncional) {
		this.siglaUnidadeFuncional = siglaUnidadeFuncional;
	}

	
	public Short getUnfSeq() {
		return unfSeq;
	}

	public void setUnfSeq(Short unfSeq) {
		this.unfSeq = unfSeq;
	}

	public Short getCodigoEspecialidade() {
		return codigoEspecialidade;
	}

	public void setCodigoEspecialidade(Short codigoEspecialidade) {
		this.codigoEspecialidade = codigoEspecialidade;
	}

	public Short getQuartoNumero() {
		return quartoNumero;
	}

	public void setQuartoNumero(Short quartoNumero) {
		this.quartoNumero = quartoNumero;
	}

	public void setRegistroNascimento(String registroNascimento) {
		this.registroNascimento = registroNascimento;
	}

	public Short getTipoCertidao() {
		return tipoCertidao;
	}

	public void setTipoCertidao(Short tipoCertidao) {
		this.tipoCertidao = tipoCertidao;
	}

	public Short getFolhas() {
		return folhas;
	}

	public void setFolhas(Short folhas) {
		this.folhas = folhas;
	}

	public Integer getTermo() {
		return termo;
	}

	public void setTermo(Integer termo) {
		this.termo = termo;
	}

	public Integer getNumeroDN() {
		return numeroDN;
	}

	public void setNumeroDN(Integer numeroDN) {
		this.numeroDN = numeroDN;
	}

	public Date getDataAtendimento() {
		return dataAtendimento;
	}

	public void setDataAtendimento(Date dataAtendimento) {
		this.dataAtendimento = dataAtendimento;
	}

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public String getIndAla() {
		return indAla;
	}

	public void setIndAla(String indAla) {
		this.indAla = indAla;
	}

	public String getAndar() {
		return andar;
	}

	public void setAndar(String andar) {
		this.andar = andar;
	}

	public Short getTciSequencia() {
		return tciSequencia;
	}

	public void setTciSequencia(Short tciSequencia) {
		this.tciSequencia = tciSequencia;
	}

	public String getCaraterInternacao() {
		return caraterInternacao;
	}

	public void setCaraterInternacao(String caraterInternacao) {
		this.caraterInternacao = caraterInternacao;
	}

	public Integer getMatriculaDigita() {
		return matriculaDigita;
	}

	public void setMatriculaDigita(Integer matriculaDigita) {
		this.matriculaDigita = matriculaDigita;
	}

	public Short getVinCodigoDigita() {
		return vinCodigoDigita;
	}

	public void setVinCodigoDigita(Short vinCodigoDigita) {
		this.vinCodigoDigita = vinCodigoDigita;
	}

	public String getNomeSerEfetuaInternacao() {
		return nomeSerEfetuaInternacao;
	}

	public void setNomeSerEfetuaInternacao(String nomeSerEfetuaInternacao) {
		this.nomeSerEfetuaInternacao = nomeSerEfetuaInternacao;
	}

	public Integer getMatriculaMedico() {
		return matriculaMedico;
	}

	public void setMatriculaMedico(Integer matriculaMedico) {
		this.matriculaMedico = matriculaMedico;
	}

	public Short getVinCodigMedico() {
		return vinCodigMedico;
	}

	public void setVinCodigMedico(Short vinCodigMedico) {
		this.vinCodigMedico = vinCodigMedico;
	}

	public String getNomeMedico() {
		return nomeMedico;
	}

	public void setNomeMedico(String nomeMedico) {
		this.nomeMedico = nomeMedico;
	}

	public String getNumeroRegConselho() {
		return numeroRegConselho;
	}

	public void setNumeroRegConselho(String numeroRegConselho) {
		this.numeroRegConselho = numeroRegConselho;
	}

	public Integer getCidSeq() {
		return cidSeq;
	}

	public void setCidSeq(Integer cidSeq) {
		this.cidSeq = cidSeq;
	}

	public String getCidsCodigo() {
		return cidsCodigo;
	}

	public void setCidsCodigo(String cidsCodigo) {
		this.cidsCodigo = cidsCodigo;
	}

	public String getCidPrincipal() {
		return cidPrincipal;
	}

	public void setCidPrincipal(String cidPrincipal) {
		this.cidPrincipal = cidPrincipal;
	}

	public BigInteger getCpfMedico() {
		return cpfMedico;
	}

	public void setCpfMedico(BigInteger cpfMedico) {
		this.cpfMedico = cpfMedico;
	}

	

	
}
