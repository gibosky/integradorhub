package br.com.tolife.integrador_hub.bean;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "logeventos")
@SequenceGenerator(name="LOGEVENTOS_ID_SEQ",sequenceName="LOGEVENTOS_ID_SEQ")
public class LogEventos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="LOGEVENTOS_ID_SEQ")
	@Column(name = "id")
	private Long id;

	@Column(name = "tipo_evento")
	private String tipoEvento;

	@Column(name = "data")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataHora;

	@Column(name = "mensagem")
	private String mensagem;

	@Column(name = "atendimento")
	private String atendimento;

	public LogEventos(String tipoEvento, String mensagem, String atendimento) {
		this.tipoEvento = tipoEvento;
		this.dataHora = new Date(System.currentTimeMillis());
		this.mensagem = mensagem;
		this.atendimento = atendimento;
	}
	
	public LogEventos() {

	}

	public String getPrintStackTrace(Exception e){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString().length() > 900 ?sw.toString().substring(1, 900):sw.toString();
	}

}

